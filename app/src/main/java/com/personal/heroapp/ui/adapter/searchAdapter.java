package com.personal.heroapp.ui.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.personal.heroapp.R;
import com.personal.heroapp.model.Search;

import java.util.ArrayList;

public class searchAdapter extends RecyclerView.Adapter<searchAdapter.ViewHolder> {
    private ArrayList<Search> mDataSet;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public ViewHolder(TextView tv) {
            super(tv);
            textView = tv;
        }
    }

    public searchAdapter() {
        mDataSet = new ArrayList<>();
    }

    public void setDataSet(ArrayList<Search> dataSet){
        mDataSet = dataSet;
        notifyDataSetChanged();
    }

    @Override
    public searchAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                       int viewType) {
        TextView tv = (TextView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_view, parent, false);

        return new ViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int i) {
        //holder.textView.setText(mDataSet.get(i).getName());
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}