package com.personal.heroapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.personal.heroapp.io.DiagnosticVetApiAdapter;
import com.personal.heroapp.model.Search;
import com.personal.heroapp.ui.adapter.searchAdapter;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity implements Callback<ArrayList<Search>> {

    private searchAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_heros);

        mRecyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        mAdapter = new searchAdapter();
        mRecyclerView.setAdapter(mAdapter);

        Call<ArrayList<Search>> call = DiagnosticVetApiAdapter.getApiService().getSearch();
        call.enqueue(this);
    }

    @Override
    public void onResponse(Call<ArrayList<Search>> call, Response<ArrayList<Search>> response) {
        if (response.isSuccessful()){
            Log.d("onResponse", "Entro => " +response);
            ArrayList<Search> search = response.body();
            Log.d("onResponse Search", "Size of Search => " + search);
            mAdapter.setDataSet(search);
        }
    }

    @Override
    public void onFailure(Call<ArrayList<Search>> call, Throwable t) {
        Log.d("onResponse", "Entro 3 => "+t);
    }
}
