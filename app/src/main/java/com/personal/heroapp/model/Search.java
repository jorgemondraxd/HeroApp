package com.personal.heroapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Search {
    @SerializedName("response")
    @Expose
    private String response;
    @SerializedName("results-for")
    @Expose
    private String results_for;
    @SerializedName("results")
    @Expose
    private ArrayList<ResultsArray> results = null;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getResults_for() {
        return results_for;
    }

    public void setResults_for(String results_for) {
        this.results_for = results_for;
    }

    public ArrayList<ResultsArray> getResults() {
        return results;
    }

    public void setResults(ArrayList<ResultsArray> results) {
        this.results = results;
    }
}
