package com.personal.heroapp.io;

import com.personal.heroapp.model.Search;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;

public interface DiagnosticVetApiService {

    @GET("search/a")
    Call<ArrayList<Search>> getSearch();
}
